# TEMA: "Conjuntos, Aplicaciones y funciones (2002)" 
 ```plantuml 
@startmindmap 
 *[#E57373] Matematicas
  *_ piezas basicas
   *[#E57373] la idea de elemento 
   *[#E57373] la idea de conjunto 
   *[#E57373] la relacion de pertenencia
  *_ se acerca mucho a la 
   *[#FF5252] filosofia
  *_ se define como
   *[#E57373] La solucion a problemas
   *[#E57373] Buscar la solucion ultima a un problema
  *_ ayuda al
   *[#FF5252] razonamiento del hombre
   *[#FF5252] razonamiento del abstracto
   *[#FF5252] dominio de las ideas
  *_ se divide en
   *[#FFCDD2] Conjuntos
    *[#E57373] cardinal de un conjunto
     *_ formula
      *[#F44336]  cardinal de la union
       *_ el cardinal de una union es igual al cardinal de 2 conjuntos
      *[#F44336]  acotacion de cardinales
       *_ basada en
        *_ quien es mayor o menor en cardinales de conjuntos
     *_ que es
      *[#B71C1C] es el numero de elementos que conforman el conjunto
    *_ graficos
     *[#B71C1C] diagramas de diagramas de venn
      *_ lo creo 
       *[#E57373] Jhon venn
      *_ recursos didacticos 
      *_ se representa mediante circulos
    *_ tipos 
     *[#F44336]  Universal 
      *_ caracteristicas
       *[#E57373] conjunto de referencia
       *[#E57373] ocurre toda la teoria
     *[#F44336]  Complejo
    *_ algunos conjuntos
     *[#B71C1C] la union 
      *_ elementos que pertenecen al menos a uno de los conjuntos
     *[#B71C1C] la interseccion 
      *_ elementos que pertenecen simultáneamente a ambos conjuntos
     *[#B71C1C] la complementacion
      *_ los que no pertenecen a un conjunto dado
     *[#B71C1C] Diferencia de conjuntos
    *_ operaciones
     *[#F44336] conjuntos complejos
     *[#F44336] armados sobre conjuntos elementales
    *[#E57373] inclusion de conjuntos
     *_ caracteristicas
      *[#F44336] nocion que existe entre 2 conjuntos
      *[#F44336] relacion de orden que existen entre numeros
      *[#F44336] un conjunto esta incluido en otro
   *[#FFCDD2] Aplicaciones
    *_ tipos 
     *[#F44336] aplicacion inyectiva
     *[#F44336] aplicacion subjetiva
     *[#F44336] aplicacion diyectiva
    *_ ideas
     *[#B71C1C] razonamiento abstracto
     *[#B71C1C] imagen a imagen inversa
     *[#B71C1C] transformacion tras transformacion
    *_ conocida como
     *[#B71C1C] clase de transformacion de un agente externo 
   *[#FFCDD2] Funciones
    *_ tambien conocida como
     *[#B71C1C] Dominio
     *[#B71C1C] condominio
     *[#B71C1C] Imagen
    *_ tipos
     *[#F44336] inyectivas
     *[#F44336] suryectivas
     *[#F44336] biyectivas
    *_ tiene 
     *[#E57373] conjunto de numeros
    *_ se define como
     *[#E57373] son aplicaciones, substratos de los numeros
     *[#E57373] son mas faciles de ver
    *_ graficas
     *_ ejemplos
      *[#B71C1C] figuras complejas
      *[#B71C1C] parabolas
     *[#E57373] Graficas de una funcion
     *[#E57373] diagramas de venn
     *[#E57373] Grafica en el plano
@endmindmap 
```


# TEMA: "Funciones (2010)" 
 ```plantuml 
@startmindmap 

 *[#FFB74D] Funciones
  *[#FFD180] ejemplos
   *[#FB8C00] una funcion es
    *_ cambio de clima
    *_ cambios relativos
    *_ cambio de conjunto de objetos
    *_ a un numero le agregas el cuadrado
  *_ consiste en
   *[#E65100] la relacion entre un elemento de un grupo y otro elemento del grupo b
    *_ siempre que 
     *[#FFD180] se vinculen de manera unica y exclusiva
  *_ concepto 
   *[#E65100] Es la relacion que hay entre una magnitud y otra
  *[#FFD180] derivada
   *_ ejemplos
    *[#FB8C00] derivada de la suma
    *[#FB8C00] derivada de un cociente
    *[#FB8C00] la regla de la cadena
    *[#FB8C00] derivadas de funciones trigonometricas
    *[#FB8C00] derivada de un producto
   *_ nace de
    *[#E65100] como aproximar una funcion relativamente \n complicada con una funcion mas simple
  *[#FFD180] derivada
   *_ crean
    *[#E65100] atajos 
    *[#E65100] reglas
    *[#E65100] maneras mas simples de resolver complejidad
   *_ que es
    *[#FF9100] cuando se consideran valores de la variable x 
     *_ cerca de
      *[#E65100] los puntos
      *[#E65100]  un determinado valor
    *_ caracteristicas
     *[#E65100]  es continua 
  *[#FFD180] Funciones crecientes
   *[#FFAB40] maximo relativo
   *_ caracteristicas
    *[#FB8C00]  aumento de la variable independiente
    *[#FB8C00]  aumentan los valores del primer conjunto
  *[#FFAB40] Limite
   *_ son 
    *[#E65100] divisiones existentes
  *[#FFAB40] Continuidad
   *_ es
    *[#E65100] la linea sobre el eje x sin ser interrumpida
  
  *[#FFD180] Funciones decrecientes
   *[#FFAB40] minimo relativo
   *_ caracteristicas
    *[#FB8C00] disminuyen los valores del primer conjunto
    *[#FB8C00]  disminuyen las variable independiente
    *[#FB8C00]  opuesto de los numeros 
  *[#FFAB40] Dominio 
   *_ es 
    *[#E65100] un conjunto de valores
     *_ de 
      *[#FFD180] entrada
      *[#FFD180] salida
  *[#FFAB40] intervalo 
   *_ es 
    *[#FFD180] un conjunto de valores
     *_ entre 
      *[#FB8C00] 2 limites dados
  *_ definicion
   *[#FFD180] son tranformaciones de un conjunto dentro de otro conjunto
  *_ ejemplo
   *[#FFD180] Una funcion que a cada numero le asignas el cuadrado
  *_ refleja
   *[#FFAB40] el pesamiento del hombre
   *[#FFAB40] lo elaborado para comprender el entorno
   *[#FFAB40] resolver problemas con ayuda de las matematicas
  *_ estudia 
   *[#FF9100] La logica
   *[#FF9100] Los numeros
   *[#FF9100] Geometria
  *_ tipos
   *[#E65100] funcion inyectiva
    *_ caracteristicas
     *[#FFAB40] los elementos del dominio A son distintos al dominio B
   *[#E65100] funcion sobreyectiva
    *_ caracteristicas
     *[#FFAB40] Comparte imagenes
     *[#FFAB40] los elementos del dominio A tienen imagen en el B
   *[#E65100] funcion biyectiva
    *_ caracteristicas 
     *[#FFAB40] no existen elementos en B que no correspondan a elementos en A
     *[#FFAB40] ocurre cuando una funcion 
      *_ es al mismo tiempo
       *[#FF9100] inyectiva
       *[#FF9100] sobreyectiva
  *_ representaciones graficas
   *[#E65100] cartesiana 
    *_ es un 
     *[#FFAB40] Conjunto de numeros reales
      *_ se crean
       *[#E65100]  Figuras
      *_ sus ejes son
       *[#E65100]  X
       *[#E65100]  Y
    *_ creada por
     *[#FFAB40] Rene Descartes
   *[#E65100] plano en un eje de numeros reales (X,Y)
   *[#E65100] recta de numeros reales (X)
 
@endmindmap 
```


# TEMA: "La matemática del computador (2002)" 

```plantuml 
@startmindmap 
 *[#4CAF50] La matemática
  *_ abstracciones
   *_ se realizaban con 
    *[#81C784] lapiz y papel
   *[#69F0AE] Numero raiz de 2
   *[#69F0AE] Numero PI con infinitas cifras decimales
  *_ cimientos de 
   *[#81C784] Los aparatos en el presente 
  *[#69F0AE] Representaciones
   *_ nos enseñan
    *[#81C784] como convertir un paso de corriente
     *_ de 
      *[#388E3C] ceros
      *[#388E3C] unos
   *[#81C784] representacion interna de numeros
   *[#81C784] representacion interna de numeros enteros
   *[#81C784] representacion magnitud signo
   *[#81C784] representacion en exceso
   *[#81C784] representacion Complemento 2
    *_ ideas matematicas
    *_ nos enseñan 
     *[#388E3C] como convertir un paso de ceros y unos en una \nserie de posiciones de memoria en un computador
  *_ aportes
   *[#69F0AE] Numero Aproximado
    *_ es cuando
     *[#81C784] un numero finito de cifras se acerca a un numero real
   *[#69F0AE] Idea de error
    *_ se comete al
     *[#81C784] aproximar un numero a un numero finito de cifras
   *[#69F0AE] Digitos significativos
   *[#69F0AE] Truncamiento un numero
    *_ conocido tambien como
     *[#81C784] truncamiento refinado
      *_ se basa en
       *[#388E3C] el error que se cometa sea lo mas pequeño posible
        *_ tiene
         *[#388E3C] error por exceso 
         *[#388E3C] error por defecto
    *_ se basa en 
     *[#81C784] cortar un numero con cifras a la derecha despues del punto
  *[#69F0AE] Aritmetica del computador
   *[#388E3C] Codificacion
    *_ aportan 
     *[#81C784] como representar las letras
     *[#81C784] como representar numeros
   *[#388E3C] Sistema octal
    *_ caracteristicas 
     *[#81C784] tiene base 8
     *[#81C784] se usa en la informatica
     *[#81C784] no requiere simbolos
   *[#388E3C] Sistema hexadecimal
    *_ caracteristicas 
     *[#81C784] tiene base 16
   *[#388E3C] Sistema binario
    *_ tiene 
     *[#388E3C] digitalizacion
      *_ es 
       *[#C8E6C9] representacion de conjuntos
      *_ crea 
       *[#C8E6C9] imagenes
       *[#C8E6C9] figuras
    *_ se construye un
     *[#388E3C] sistema de numeracion
    *_ enlaza con
     *[#388E3C] la logica booleana
     *[#388E3C] pertenencia en conjuntos
     *[#388E3C] no pertenencia en conjuntos
    *_ reprensentan
     *[#C8E6C9] un bit 
      *_ compuesto de
       *[#69F0AE] ceros y unos
    *_ ideas
     *[#C8E6C9] adicion binaria
      *_ se basa en 
       *[#388E3C] sumar en base dos
       *[#388E3C] misma logica que la adicion decimal
    *_ se basa en 
     *[#81C784] Es un sistema simple ceros y uno
     *[#81C784] cero y uno
     *[#81C784] Se crea un sistema de numeracion
   *_ numeros
    *[#388E3C] Abstracción de numeros
    *[#388E3C] numeros reales
    *[#388E3C] Funciones
    *[#388E3C] Calculos
@endmindmap 
```
# "Autor: Lopez Rodriguez Hector Aldahir" 

### Para la elaboracion de estos mapas conceptuales se usaron los formatos Mindmap y PlantUML .
#### Herramientas
##### PlantUML
[plantuml @ plantuml](https://docs.gitlab.com/ee/administration/integration/plantuml.html)
##### MindMap
[mindmap @ mindmap](https://plantuml.com/es/mindmap-diagram)

